@extends('layouts_admin.header')
@section('content')
    <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item">Items</li>
        <li class="breadcrumb-item"><a href="#">Users</a></li>
    </ul>
    </div>
    <form method="post" action="{{ route('users.store') }}">
        @csrf
        <div class="tile">

            <h3 class="title" align="center">New User</h3><br>


            <div class="tile-body">
                <input class="form-control" id="" name="name" type="text" placeholder="Enter User Name ..">
            </div>
            <br>
            <div class="tile-body">
                <input class="form-control" id="" name="email" type="email" placeholder="Enter User Email ..">
            </div>
            <br>
            <div class="tile-body">
                <select name="gender" id="gender" style="padding: 10px; border-radius: 10px;">
                    <option value="0"> Male </option>
                    <option value="1"> Female </option>
                </select>
            </div>
            <br>
            <div class="tile-body">
                <input class="form-control" id="" name="password" type="password" placeholder="Enter User Password ..">
            </div>
            <br>
            <div class="tile-body">
                <input class="form-control" id="" name="password_confirmation" type="password" placeholder="Confirm Password ..">
            </div>
            <br>

            <div class="tile-footer" align="center">
                <button class="btn btn-primary" name="submit" type="submit"> Add </button>
            </div>



        </div>
        @include('layouts_admin.error')
    </form>
@endsection







