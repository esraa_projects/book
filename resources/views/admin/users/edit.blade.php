@extends('layouts_admin.header')
@section('content')
    <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item">Items</li>
        <li class="breadcrumb-item"><a href="#">Users</a></li>
    </ul>
    </div>
    <form method="post" action="{{ route('users.update', $user->id) }}">
        @method('PATCH')
        @csrf
        <div class="tile">

            <h3 class="title" align="center">Edit User</h3><br>


            <div class="tile-body">
                <input class="form-control" id="" name="name" type="text" value={{ $user->name }}>
            </div>
            <br>
            <div class="tile-body">
                <input class="form-control" id="" name="email" type="email" value={{ $user->email }}>
            </div>
            <br>
            <div class="tile-body">
                @if ($user->gender == 0 )
                    <select name="gender" id="gender" style="padding: 10px; border-radius: 10px;">
                        <option selected value="0"> Male </option>
                        <option value="1"> Female </option>
                    </select>
                @endif
                    @if ($user->gender == 1 )
                        <select name="gender" id="gender" style="padding: 10px; border-radius: 10px;">
                            <option  value="0"> Male </option>
                            <option selected value="1"> Female </option>
                        </select>
                    @endif

            </div>
            <br>
            <div class="tile-body">
                <input class="form-control" id="" name="password" type="password" placeholder="Change User Password ..">
            </div>
            <br>
            <div class="tile-body">
                <input class="form-control" id="" name="password_confirmation" type="password" placeholder="Confirm User Password ..">
            </div>
            <br>

            <div class="tile-footer" align="center">
                <button class="btn btn-primary" name="submit" type="submit"> Edit </button>
            </div>



        </div>
        @include('layouts_admin.error')
    </form>
@endsection







