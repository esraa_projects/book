@extends('layouts_admin.header')

@section('content')
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-shopping-cart fa-3x"></i>
            <div class="info">
              <h4>Orders</h4>
              <p><b>50</b></p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="widget-small info coloured-icon"><i class="icon fa fa-envelope fa-3x"></i>
            <div class="info">
              <h4>Messages</h4>
              <p><b>4</b></p>
            </div>
          </div>
        </div>
        <div class="col-md-4 ">
          <div class="widget-small warning coloured-icon"><i class="icon fa fa-users fa-3x"></i>
            <div class="info">
              <h4>Table Booking</h4>
              <p><b>5</b></p>
            </div>
          </div>
        </div>
	</div>
       <div class="row">
        
        <div class="col-md-4">
          <div class="widget-small danger coloured-icon"><i class="icon fa fa-calendar fa-3x"></i>
            <div class="info">
              <h4>Events</h4>
              <p><b>6</b></p>
            </div>
          </div>
        </div>
      
      
        <div class="col-md-4">
          <div class="widget-small warning coloured-icon"><i class="icon fa fa-comment fa-3x"></i>
            <div class="info">
              <h4>Reviews</h4>
              <p><b>3</b></p>
            </div>
          </div>
	</div>
        
        <div class="col-md-4">
		  <div class="widget-small danger coloured-icon"><i class="icon fa fa-money fa-3x"></i>
            <div class="info">
              <h4>Order Servies</h4>
              <p><b>10 $</b></p>
            </div>
          </div>
        </div>
	</div>
@endsection   
 
