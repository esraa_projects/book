@extends('layouts_admin.header')
@section('content')
    <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item">Items</li>
        <li class="breadcrumb-item"><a href="#">Books</a></li>
    </ul>
    </div>
    <form method="post" action="{{ route('books.update', $book->id) }}" enctype="multipart/form-data">
        @method('PATCH')
        @csrf
        <div class="tile">

            <h3 class="title" align="center">Edit Book</h3><br>


            <div class="tile-body">
                <input class="form-control" id="" name="title" type="text" value={{ $book->title }}>
            </div>
            <br>
            <div class="tile-body">
                <textarea class="form-control" id="" name="description" type="text">{{ $book->description }}</textarea>
            </div>
            <br>
            <div class="tile-body">
                <input class="form-control" id="" name="photo" type="file" value={{ $book->photo }}>
            </div>
            <br>
            <div class="" >
                <select name="author" id="" style="padding: 10px; border-radius: 10px;">
                    @foreach($authors as $author)
                        <option value="{{$author->id}}"> {{$author->name}} </option>
                    @endforeach
                </select>
            </div>
            <br>
            <div class="" >
                <select name="cat" id="" style="padding: 10px; border-radius: 10px;" >
                    @foreach($categories as $category)
                        <option value="{{$category->id}}"> {{$category->name}} </option>
                    @endforeach
                </select>
            </div>
            <br>
            <div class="tile-body">
                <input class="form-control" id="" name="sn" type="text" value={{ $book->publish_sn }}>
            </div>
            <br>

            <div class="tile-footer" align="center">
                <button class="btn btn-primary" name="submit" type="submit"> Edit </button>
            </div>



        </div>
        @include('layouts_admin.error')
    </form>
@endsection







