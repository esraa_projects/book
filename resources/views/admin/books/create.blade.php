@extends('layouts_admin.header')
@section('content')
    <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item">Items</li>
        <li class="breadcrumb-item"><a href="#">Books</a></li>
    </ul>
    </div>
    <form method="post" action="{{ route('books.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="tile">

            <h3 class="title" align="center">New Book</h3><br>


            <div class="tile-body">
                <input class="form-control" id="" name="title" type="text" placeholder="Enter Book Title ..">
            </div>
            <br>
            <div class="tile-body">
                <textarea class="form-control" id="" name="description" type="text" placeholder="Enter Book Description .."></textarea>
            </div>
            <br>
            <div class="tile-body">
                <input class="form-control" id="" name="photo" type="file">
            </div>
            <br>
            <div class="" >
                <select name="author" id="" style="padding: 10px; border-radius: 10px;">
                    @foreach($authors as $author)
                        <option value="{{$author->id}}"> {{$author->name}} </option>
                    @endforeach
                </select>
            </div>
            <br>
            <div class="" >
                <select name="cat" id="" style="padding: 10px; border-radius: 10px;" >
                    @foreach($categories as $category)
                    <option value="{{$category->id}}"> {{$category->name}} </option>
                    @endforeach
                </select>
            </div>
            <br>
            <div class="tile-body">
                <input class="form-control" id="" name="sn" type="text" placeholder="Enter Book Serial Number ..">
            </div>
            <br>

            <div class="tile-footer" align="center">
                <button class="btn btn-primary" name="submit" type="submit"> Add </button>
            </div>



        </div>
        @include('layouts_admin.error')
    </form>
@endsection







