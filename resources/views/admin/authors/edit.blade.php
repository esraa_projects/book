@extends('layouts_admin.header')
@section('content')
    <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item">Items</li>
        <li class="breadcrumb-item"><a href="#">Authors</a></li>
    </ul>
    </div>
    <form method="post" action="{{ route('authors.update', $author->id) }}">
        @method('PATCH')
        @csrf
        <div class="tile">

            <h3 class="title" align="center">Edit Category</h3>


            <div class="tile-body">
                <input class="form-control" id="" name="name" type="text" value={{ $author->name }}>
            </div>
            <br>
            <div class="tile-body">
                <textarea class="form-control" id="" name="description" type="text" >{{ $author->description }}</textarea>
            </div>
            <div class="tile-footer" align="center">
                <button class="btn btn-primary" name="submit" type="submit"> Edit </button>
            </div>



        </div>
        @include('layouts_admin.error')
    </form>
@endsection







