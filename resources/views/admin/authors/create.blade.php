@extends('layouts_admin.header')
@section('content')
    <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item">Items</li>
        <li class="breadcrumb-item"><a href="#">Authors</a></li>
    </ul>
    </div>
    <form method="post" action="{{ route('authors.store') }}">
        @csrf
        <div class="tile">

            <h3 class="title" align="center">New Author</h3><br>


            <div class="tile-body">
                <input class="form-control" id="" name="name" type="text" placeholder="Enter Author Name ..">
            </div>
            <br>
            <div class="tile-body">
                <textarea class="form-control" id="" name="description" type="text" placeholder="Enter Author Description .."></textarea>
            </div>
            <div class="tile-footer" align="center">
                <button class="btn btn-primary" name="submit" type="submit"> Add </button>
            </div>



        </div>
        @include('layouts_admin.error')
    </form>
@endsection







