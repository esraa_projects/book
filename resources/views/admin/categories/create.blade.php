@extends('layouts_admin.header')
@section('content')
    <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item">Items</li>
        <li class="breadcrumb-item"><a href="#">Category</a></li>
    </ul>
    </div>
    <form method="post" action="{{ route('categories.store') }}">
        @csrf
        <div class="tile">

            <h3 class="title" align="center">New Category</h3>


            <div class="tile-body">
                <input class="form-control" id="" name="cat" type="text" placeholder="Enter Category Name ..">
            </div>
            <div class="tile-footer" align="center">
                <button class="btn btn-primary" name="submit" type="submit"> Add </button>
            </div>



        </div>
        @include('layouts_admin.error')
    </form>
@endsection







