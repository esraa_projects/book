<html class="no-js" lang=""> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Book Library</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="{{ asset('images/apple-touch-icon.png') }}">
	<link rel="stylesheet" href="{{ asset('css/book/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/book/normalize.css') }}">
	<link rel="stylesheet" href="{{ asset('css/book/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/book/icomoon.css') }}">
	<link rel="stylesheet" href="{{ asset('css/book/jquery-ui.css') }}">
	<link rel="stylesheet" href="{{ asset('css/book/owl.carousel.css') }}">
	<link rel="stylesheet" href="{{ asset('css/book/transitions.css') }}">
	<link rel="stylesheet" href="{{ asset('css/book/main.css') }}">
	<link rel="stylesheet" href="{{ asset('css/book/color.css') }}">
	<link rel="stylesheet" href="{{ asset('css/book/responsive.css') }}">
	<script src="{{ asset('js/book/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
</head>
<body class="tg-home tg-homeone">
	
	<!--************************************
			Wrapper Start
	*************************************-->
	<div id="tg-wrapper" class="tg-wrapper tg-haslayout">
		<!--************************************
				Header Start
		*************************************-->
		<header id="tg-header" class="tg-header tg-haslayout">
			@if(Auth::check())

			<div class="tg-topbar">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							
							<div class="dropdown tg-themedropdown tg-currencydropdown">
								<a href="javascript:void(0);" id="tg-currenty" class="tg-btnthemedropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="icon-user"></i>
									<span>Account</span>
								</a>
								<ul class="dropdown-menu tg-themedropdownmenu" aria-labelledby="tg-currenty">

									<li>
										<a href="{{ url ('/logout')}}">
											
											<span>logout</span>
										</a>
									</li>
									
								</ul>
							</div>
							<div class="tg-userlogin">
								<figure><img style = "width: 20px " src="{{ asset('images/person.jpg')}}" alt="image description"></figure>
								<b > <i>  Hi, {{ Auth::user()->name }}</i></b>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endif
			<div class="tg-middlecontainer">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<strong class="tg-logo"><a href="{{ url ('/')}}"><img src="{{ asset('images/logo.png')}}" alt="company name here"></a></strong>
							
							<div class="tg-searchbox">
								<form class="tg-formtheme tg-formsearch">
									<fieldset>
										<input type="text" name="search" class="typeahead form-control" placeholder="Search by title, author, keyword, ISBN...">
										<button type="submit"><i class="icon-magnifier"></i></button>
									</fieldset>
									
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tg-navigationarea">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<nav id="tg-nav" class="tg-nav">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#tg-navigation" aria-expanded="false">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
								<div id="tg-navigation" class="collapse navbar-collapse tg-navigation">
									<ul>
										<li class="">
											<a href="{{ url ('/')}}">Home</a>
											
										</li>
										<li class="">
											<a href="{{ url ('/about')}}">About us</a>
											
										</li>
										<li class="">
											<a href="{{ url ('/authors')}}">Authors</a>
											
										</li>
										<li><a href="{{ url ('/books')}}">Books</a></li>
										
										
										<li><a href="{{ url ('/contact')}}">Contact</a></li>
										@if( ! Auth::check())
										<li><a href="{{ url ('/login')}}">Login</a></li>
										<li><a href="{{ url ('/register')}}">Register</a></li>
											@endif
										
									</ul>
								</div>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!--************************************
				Header End
		*************************************-->
@yield('content')
@include('layouts_books.footer')
		