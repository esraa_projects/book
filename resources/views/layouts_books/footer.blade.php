<!--************************************
				Footer Start
		*************************************-->
		<footer id="tg-footer" class="tg-footer tg-haslayout">
			<div class="tg-footerbar">
				<a id="tg-btnbacktotop" class="tg-btnbacktotop" href="javascript:void(0);"><i class="icon-chevron-up"></i></a>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<span class="tg-paymenttype"><img src="{{ asset('images/paymenticon.png') }}" alt="image description"></span>
							<span class="tg-copyright">2018 All Rights Reserved By &copy; Book Library</span>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!--************************************
				Footer End
		*************************************-->
	</div>
	<!--************************************
			Wrapper End
	*************************************-->
	<script src="{{ asset('js/book/vendor/jquery-library.js')}}"></script>
	<script src="{{ asset('js/book/vendor/bootstrap.min.js')}}"></script>
	<script src="{{ asset('js/book/js_key_AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus_language_en.js')}}"></script>
	<script src="{{ asset('js/book/owl.carousel.min.js')}}"></script>
	<script src="{{ asset('js/book/jquery.vide.min.js')}}"></script>
	<script src="{{ asset('js/book/countdown.js')}}"></script>
	<script src="{{ asset('js/book/jquery-ui.js')}}"></script>
	<script src="{{ asset('js/book/parallax.js')}}"></script>
	<script src="{{ asset('js/book/countTo.js')}}"></script>
	<script src="{{ asset('js/book/appear.js')}}"></script>
	<script src="{{ asset('js/book/gmap3.js')}}"></script>
	<script src="{{ asset('js/book/main.js')}}"></script>
</body>
</html>