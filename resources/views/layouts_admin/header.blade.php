@if(Auth::check() && Auth::user()->admin == '1')
<!DOCTYPE html>
 <html lang="en">
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Admin - Control Pannel</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    
    
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="">Project Admin</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="{{ url ('/admin/logout')}}"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="{{ asset ('images/person.jpg') }}" alt="User Image" style="width:40px;">
        <div>
          <p class="app-sidebar__user-name">Esraa Mostafa</p>
          <p class="app-sidebar__user-designation">Back end Developer</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item active" href="{{ url('/admin/dash')}}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
        <li><a class="app-menu__item" href="{{ url('/admin/categories')}}" ><i class="app-menu__icon fa fa-circle-o"></i><span class="app-menu__label">Categories</span></a></li>
        <li><a class="app-menu__item" href="{{ url('/admin/authors')}}" ><i class="app-menu__icon fa fa-circle-o"></i><span class="app-menu__label">Authors</span></a></li>
        <li><a class="app-menu__item" href="{{ url('/admin/books')}}" ><i class="app-menu__icon fa fa-circle-o"></i><span class="app-menu__label">Books</span></a></li>
        <li><a class="app-menu__item" href="{{ url('/admin/users')}}" ><i class="app-menu__icon fa fa-circle-o"></i><span class="app-menu__label">Users</span></a></li>
      </ul>
    </aside>

       <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p>Project Admin - Control Pannel</p>
        </div>
<!-- Page Content -->


    
    @yield('content')
      @include('layouts_admin.footer')
@endif
@if(! Auth::check() or Auth::user()->admin == '0')
          you don't have permission to view this page .. please go back or login as admin to view this page
 @endif

       