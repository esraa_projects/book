@extends('layouts_books.header')

@section('content') 
		<!--************************************
				Main Start
		*************************************-->
		<div style="" >
		<main >
				<!--************************************
					Picked By Author Start
			*************************************-->
			<section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="tg-sectionhead">
								<h2><span>Some Great Books</span>Picked By Authors</h2>
								
							</div>
						</div>
						<div id="tg-pickedbyauthorslider" class="tg-pickedbyauthor tg-pickedbyauthorslider owl-carousel">
							@foreach($books as $book)
							<div class="item">
								<div class="tg-postbook">
									<figure class="tg-featureimg">
										<div class="tg-bookimg">
											<div class="tg-frontcover"><img src="{{ asset($book->photo) }}" alt="image description"></div>
										</div>
										<div class="tg-hovercontent">
											<div class="tg-description">
												<p>{{$book->description}}</p>
											</div>

											<strong class="tg-bookcategory">Category: {{$book->category->name}}</strong>
											
											<div class="tg-ratingbox"><span class="tg-stars"><span></span></span></div>
										</div>
									</figure>
									<div class="tg-postbookcontent">
										<div class="tg-booktitle">
											<h3><a href="{{ route('book.show', $book->id) }}">{{$book->title}}</a></h3>
										</div>
										<span class="tg-bookwriter">By: <a href="{{ route('author.show', $book->author_id) }}">{{$book->author->name}}</a></span>
										
									</div>
								</div>
							</div>
								@endforeach

						</div>
					</div>
				</div>
			</section>
			<!--************************************
					Picked By Author End
			*************************************-->
			
			
			
		</main></div>
		<!--************************************
				Main End
		*************************************-->
		@endsection