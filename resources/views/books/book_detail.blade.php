@extends('layouts_books.header')

@section('content') 
			<!--************************************
				Inner Banner Start
		*************************************-->
		<div class="tg-innerbanner tg-haslayout tg-parallax tg-bginnerbanner" data-z-index="-100" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ asset('images/parallax/bgparallax-07.jpg ') }}">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="tg-innerbannercontent">
							<h1>All Details</h1>
							<ol class="tg-breadcrumb">
								<li><a href="javascript:void(0);">home</a></li>
								<li><a href="javascript:void(0);">Books</a></li>
								<li class="tg-active">Book Detail Here</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--************************************
				Inner Banner End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->
		<main id="tg-main" class="tg-main tg-haslayout">
			<!--************************************
					News Grid Start
			*************************************-->
			<div class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div id="tg-twocolumns" class="tg-twocolumns">
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 pull-right">
								<div id="tg-content" class="tg-content">
									
									<div class="tg-productdetail">
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
												<div class="tg-postbook">
													<figure class="tg-featureimg"><img src="{{ asset($book->photo) }}" alt="image description"></figure>
													
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
												<div class="tg-productcontent">
													<ul class="tg-bookscategories">
														<li><a href="{{ route('category.show', $book->category_id) }}">{{ $book->category->name }}</a></li>
													</ul>
													
													<div class="tg-booktitle">
														<h3>{{ $book->title }}</h3>
													</div>
													<span class="tg-bookwriter">By: <a href="javascript:void(0);">{{ $book->author->name }}</a></span>
													<span class="tg-stars"><span></span></span>
													
													<div class="tg-share">
														<span>Share:</span>
														<ul class="tg-socialicons">
															<li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
															<li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
															<li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
															<li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
															<li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li>
														</ul>
													</div>
													<div class="tg-description">
														<p> {{ $book->description }}</p>
													</div>
													<div class="tg-sectionhead">
														<h2>Book Details</h2>
													</div>
													<ul class="tg-productinfo">



														<li><span>Publication Date:</span><span>{{ $book->published_at->format('Y-M-d') }}</span></li>



														<li><span>SN:</span><span>{{ $book->publish_sn }}</span></li>


													</ul>
													
												</div>
											</div>
											
											<div class="tg-aboutauthor">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="tg-sectionhead">
														<h2>About Author</h2>
													</div>
													<div class="tg-authorbox">
														<figure class="tg-authorimg">
															<img style="width: 100px" src="{{ asset('images/person.jpg') }}" alt="image description">
														</figure>
														<div class="tg-authorinfo">
															<div class="tg-authorhead">
																<div class="tg-leftarea">
																	<div class="tg-authorname">
																		<h2>{{ $book->author->name }}</h2>
																		<span>Author Since: {{ $book->published_at->format('Y-M-d') }}</span>
																	</div>
																</div>
																<div class="tg-rightarea">
																	<ul class="tg-socialicons">
																		<li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
																		<li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
																		<li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
																		<li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
																		<li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li>
																	</ul>
																</div>
															</div>
															<div class="tg-description">
																<p>{{ $book->author->description }} </p>
															</div>
															<a class="tg-btn tg-active" href="{{ route('author.show', $book->author_id) }}">View All Books</a>
														</div>
													</div>
												</div>
											</div>
											
											</div>
										</div>
									</div>
								</div>
								
							
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 pull-left">
								<aside id="tg-sidebar" class="tg-sidebar">
									<div class="tg-widget tg-widgetsearch">
										<form class="tg-formtheme tg-formsearch">
											<div class="form-group">
												<button type="submit"><i class="icon-magnifier"></i></button>
												<input type="search" name="search" class="form-group" placeholder="Search by title, author, key...">
											</div>
										</form>
									</div>
									<div class="tg-widget tg-catagories">
										<div class="tg-widgettitle">
											<h3>Categories</h3>
										</div>
										<div class="tg-widgetcontent">
											<ul>
												@foreach($cats as $cat)
												<li><a href="{{ route('category.show', $cat->id) }}"><span>{{$cat->name}}</span></a></li>
												@endforeach
												
											</ul>
										</div>
									</div>
								</aside>
								
								
						</div>
					</div>
				</div>
			</div>
			</div>
			<!--************************************
					News Grid End
			*************************************-->
		</main>
		<!--************************************
				Main End
		*************************************-->
		@endsection