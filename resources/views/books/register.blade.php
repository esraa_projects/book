@extends('layouts_books.header')

@section('content') 
			<!--************************************
				Inner Banner Start
		*************************************-->
		
		<div class="tg-innerbanner tg-haslayout tg-parallax tg-bginnerbanner" data-z-index="-100" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ asset('images/parallax/bgparallax-07.jpg ') }}">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="tg-innerbannercontent">
							<h1>register</h1> <br>
							<ol class="tg-breadcrumb">
								<li><a href="javascript:void(0);">home</a></li>
								<li class="tg-active">Register</li>
							</ol>
						</div>
					</div>
				</div>
				
			</div>
	</div> 
		<!--************************************
				Inner Banner End 

		*************************************-->
		<main id="tg-main" class="tg-main tg-haslayout">
			<!--************************************
					Contact Us Start
			*************************************-->
			<div class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="tg-contactus">
							
							
							
							<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
								<div class="row">
						<div class="tg-contactus" >
						
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<div style="margin-left: 65% ; width: 200%;  margin-top: -30% ; " >
								<form class="tg-formtheme tg-formcontactus" action="{{ route('register.form') }}" method="post">
									@csrf
								
									<fieldset>
									
										<div class="form-group">
											<input type="text" name="name" class="form-control" placeholder="Enter your name .." required>
										</div><p></p>
										
										<div class="form-group">
											<input type="email" name="email" class="form-control" placeholder="Enter your e-mail .." required>
										</div><p></p>
										<div class="form-group">
											<select name="gender" id="gender">
												<option value="0"> Male </option>
												<option value="1"> Female </option>
											</select>
										</div><p></p>
										
										<div class="form-group">
											<input type="password" name="password" class="form-control" placeholder="Enter your password .." required>
										</div> <p></p>
										<div class="form-group">
											<input type="password" name="password_confirmation" class="form-control" placeholder="Enter your password again .." required>
										</div> <p></p>
										
										<div class="form-group" >
											<button name="register" type="submit" class="tg-btn tg-active">Register</button>
										</div>
										@include('layouts_books.error')
									</fieldset>
									
									
								</form>
								
					
							</div>
						</div>
					</div>
				</div>
							</div></div> </div></div></div>
			<!--************************************
					Contact Us End
			*************************************-->
		</main>
		<!--************************************
				Main End
		*************************************-->
		@endsection