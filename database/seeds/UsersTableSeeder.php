<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'esraa',
            'email' => 'esraa@gmail.com',
            'password' => bcrypt('password'),
            'gender' => '1',
            'admin' => '1',
            'rememberToken'=> '',
        ]);
    }
}
