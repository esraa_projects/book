<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    public function books(){
        return $this->morphMany(Book::class, 'category');
    }
}
