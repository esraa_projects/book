<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function category()
    {
        return $this->belongsTo(Categories::class);
    }
    public function author()
    {
        return $this->belongsTo(Author::class);
    }
    protected $dates = ['published_at'];
}
