<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User ;

class registercontroller extends Controller
{
    public  function  create(){
        return view('books.register');
    }
    public  function  store(Request $request){

        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'gender' => 'required',
            'password'=>'required|confirmed'
        ]);
        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->gender = $request->get('gender');
        $user->admin = '0';
        $user->password = bcrypt($request->get('password'));
        $user->remember_token = '';
        $user->save();
        auth()->login($user);
        return redirect('/');
    }

}
