<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Author;

class authorcontroller extends Controller
{
    public function show_all()
    {
        $authors = Author::all();

        return view('books.author', compact('authors'));
    }
    public function show($id)
    {
        $author = Author::find($id);

        return view('books.author_detail', compact('author'));
    }
    public function index()
    {
        $authors = Author::all();

        return view('admin.authors.index', compact('authors'));
    }
    public function create()
    {
        return view('admin.authors.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'description'=>'required',
        ]);
        $author = new Author ;
        $author->name = $request->get('name');
        $author->description = $request->get('description');
        $author->save();
        return redirect('/admin/authors')->with('success', 'Author has been added');
    }
    public function edit($id)
    {
        $author = Author::find($id);

        return view('admin.authors.edit', compact('author'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'description'=>'required',

        ]);

        $author = Author::find($id);
        $author->name = $request->get('name');
        $author->description = $request->get('description');
        $author->save();

        return redirect('/admin/authors')->with('success', 'Author has been updated');
    }
    public function destroy($id)
    {
        $author = Author::find($id);
        $author->delete();

        return redirect('/admin/authors')->with('success', 'Author has been deleted Successfully');
    }

}
