<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;

class categorycontroller extends Controller
{
    public function index()
    {
        $categories = Categories::all();

        return view('admin.categories.index', compact('categories'));
    }
    public function show($id)
    {
        $cat = Categories::find($id);
        return view('books.category_detail', compact('cat'));
    }
    public function create()
    {
        return view('admin.categories.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'cat'=>'required',
        ]);
        $category = new Categories ;
        $category->name = $request->get('cat');
        $category->save();
        return redirect('/admin/categories')->with('success', 'Category has been added');
    }
    public function edit($id)
    {
        $category = Categories::find($id);

        return view('admin.categories.edit', compact('category'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'cat'=>'required',

        ]);

        $category = Categories::find($id);
        $category->name = $request->get('cat');
        $category->save();

        return redirect('/admin/categories')->with('success', 'Category has been updated');
    }
    public function destroy($id)
    {
        $category = Categories::find($id);
        $category->delete();

        return redirect('/admin/categories')->with('success', 'Category has been deleted Successfully');
    }
}
