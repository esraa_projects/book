<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User ;


class sessioncontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('guest' , ['except'=>'destroy']);
    }
    public function store(){

        if(! auth()->attempt(request(['email','password'])) ) {
            return back()->withErrors([
                'message' => 'Please check your email or password and try again'

            ]);
        }
         return redirect('/');


    }

    public function create(){
        return view('books.login');

    }


    public  function  destroy(){
        auth()->logout();
        return redirect('/');

    }
}
