<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Author;
use App\User;
use App\Book;

class bookcontroller extends Controller
{
    public function show_all()
    {
        $books = Book::all();

        return view('books.book', compact('books'));
    }
    public function show($id)
    {
        $book = Book::find($id);
        $cats = Categories::all();

        return view('books.book_detail', compact('book','cats'));
    }
    public function index()
    {
        $books = Book::all();

        return view('admin.books.index', compact('books'));
    }
    public function create()
    {
        $categories = Categories::all();
        $authors = Author::all();
        return view('admin.books.create', compact('categories','authors'));

    }
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'description'=>'required',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'author'=>'required',
            'cat'=>'required',
            'sn'=>'required',
        ]);
        $imageName = time().'.'.$request->photo->getClientOriginalExtension();
        $request->photo->move(public_path('images'), $imageName);

        $book = new Book ;
        $book->title = $request->get('title');
        $book->description = $request->get('description');
        $book->photo = 'images/'.$imageName;
        $book->author_id = $request->get('author');
        $book->category_id = $request->get('cat');
        $book->author_type = 'App\Author';
        $book->category_type = 'App\Categories';
        $book->user_id = '1';
        $book->published_at = now();
        $book->publish_sn = $request->get('sn');
        $book->save();
        return redirect('/admin/books/')->with('success', 'Book has been added');
    }
    public function edit($id)
    {
        $book = Book::find($id);
        $categories = Categories::all();
        $authors = Author::all();

        return view('admin.books.edit', compact('book','categories','authors'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=>'required',
            'description'=>'required',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'author'=>'required',
            'cat'=>'required',
            'sn'=>'required',

        ]);
        $imageName = time().'.'.$request->photo->getClientOriginalExtension();
        $request->photo->move(public_path('images'), $imageName);

        $book = Book::find($id);
        $book->title = $request->get('title');
        $book->description = $request->get('description');
        $book->photo = 'images/'.$imageName;
        $book->author_id = $request->get('author');
        $book->category_id = $request->get('cat');
        $book->user_id = '1';
        $book->published_at = now();
        $book->publish_sn = $request->get('sn');
        $book->save();

        return redirect('/admin/books')->with('success', 'Book has been updated');
    }
    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();

        return redirect('/admin/books')->with('success', 'Book has been deleted Successfully');
    }
}
