<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('books.home');
});
Route::get('/contact', function () {
    return view('books.contact');
});
Route::get('/about', function () {
    return view('books.about');
});
Route::get('/login','sessioncontroller@create' );
Route::post('/login','sessioncontroller@store' )->name('login.form');
Route::get('/register','registercontroller@create' );
Route::post('/register','registercontroller@store' )->name('register.form');
Route::get('/logout','sessioncontroller@destroy' );
Route::get('/books','bookcontroller@show_all' );
Route::get('/books/show/{id}','bookcontroller@show' )->name('book.show');
Route::get('/authors','authorcontroller@show_all' );
Route::get('/authors/show/{id}', 'authorcontroller@show')->name('author.show');
Route::get('/category/show/{id}', 'categorycontroller@show')->name('category.show');
Route::get('/admin/logout','sessioncontroller@destroy' );
Route::resource('admin/categories', 'categorycontroller');
Route::resource('admin/authors', 'authorcontroller');
Route::resource('admin/books', 'bookcontroller');
Route::resource('admin/users', 'usercontroller');

Route::get('/admin', function () {
    return view('admin.dash');
});




